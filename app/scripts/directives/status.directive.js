﻿(function () {
    'use strict';

    angular
      .module('funcef-status.directive')
      .directive('ngfStatus', ngfStatus);

    /* @ngInject */
    function ngfStatus() {
        return {
            restrict: 'EA',
            templateUrl: 'views/status.view.html',
            scope: {
                status: '='
            },
            link: function (scope, element, attr, ctrl) {
                scope.$watch('status', function () {
                    
                });
            }

        }
    }
})();