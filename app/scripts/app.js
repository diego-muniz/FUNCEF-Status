(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name status
    * @version 1.0.0
    * @Componente para situacao
    */
    angular.module('funcef-status.controller', []);
    angular.module('funcef-status.directive', []);

    angular
    .module('funcef-status', [
      'funcef-status.controller',
      'funcef-status.directive'
    ]);
})();