# Componente - FUNCEF-Status

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Status mostra a informação de situação (Não Alterado, Novo, Alterado, Remover).

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-status --save.
```

## Script

```html
<script src="bower_components/funcef-status/dist/funcef-status.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-status/dist/funcef-status.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funfef-status dentro do módulo do Sistema.

```js
angular
    .module('funcef-demo', ['funcef-status']);
```

## Uso

##### Controller

```
"new","edit", "delete", ""
Novo, Alterado, Remover, Não Alterado
```

```js
vm.obj = {status: "new"};
```

##### View

```html
<tr ng-repeat="item in vm.obj">
    <td ngf-status status=item.status></td>
</tr>
```

## Desinstalação:

```
bower uninstall funcef-status --save
```
