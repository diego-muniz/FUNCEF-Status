﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;

		vm.obj = {nome: "", status: ""};

		vm.obj = [{nome: "João", status: "new"}, 
		{nome: "Maria", status:"edit"},
		{nome:"Paulo", status: "delete"},
		{nome:"Marcos", status:""}];
    }
}());