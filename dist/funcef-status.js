(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name status
    * @version 1.0.0
    * @Componente para situacao
    */
    angular.module('funcef-status.controller', []);
    angular.module('funcef-status.directive', []);

    angular
    .module('funcef-status', [
      'funcef-status.controller',
      'funcef-status.directive'
    ]);
})();;(function () {
    'use strict';

    angular
      .module('funcef-status.directive')
      .directive('ngfStatus', ngfStatus);

    /* @ngInject */
    function ngfStatus() {
        return {
            restrict: 'EA',
            templateUrl: 'views/status.view.html',
            scope: {
                status: '='
            },
            link: function (scope, element, attr, ctrl) {
                scope.$watch('status', function () {
                    
                });
            }

        }
    }
})();;angular.module('funcef-status').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/status.view.html',
    "<span class=\"label label-default\" ng-if=\"!status\">Não Alterado</span> <span class=\"label label-success\" ng-if=\"status == 'new'\">Novo</span> <span class=\"label label-info\" ng-if=\"status == 'edit'\">Alterado</span> <span class=\"label label-danger\" ng-if=\"status == 'delete'\">Remover</span>"
  );

}]);
